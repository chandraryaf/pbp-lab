from django.shortcuts import render, redirect
from lab_2.models import Note
from django.contrib.auth.decorators import login_required
from .forms import NoteForm


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    data = Note.objects.all() # TODO Implement this
    response = {'note': data}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    forms = NoteForm(request.POST or None)
    if request.method == 'POST':
        if forms.is_valid():
            forms.save()
            return redirect('/lab-4/')
    
    
    response = {'forms':forms}
    return render(request, 'lab4_form.html', response)

@login_required(login_url='/admin/login/')
def note_list(request):
    note = Note.objects.all().values()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)

