1. Apakah perbedaan antara JSON dan XML? | What is the difference between JSON and XML?<br>
- JSON is based on JS language. XML is derived from SGML<br>
- JSON is a way to represent object. XML is a markup language and uses tag structure to represent data items.<br>
- JSON does not support for namespaces. XML provide support for namespaces.<br>
- JSON support array. XML does not support array.<br>

2. Apakah perbedaan antara HTML dan XML | What is the difference between HTML and XML?<br>
HTML and XML are related to each other, where HTML displays data and describes the structure of a webpage, whereas XML stores and transfers data. HTML is a simple predefined language, while XML is a standard language that defines other languages. 