import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home:D'),
      ),
      body: Center(
        child: Text(
              'Welcome to Mutuals👋 ',
              style: TextStyle(color: Colors.black, fontSize: 25),
            ),
          ),
    );

  }
}