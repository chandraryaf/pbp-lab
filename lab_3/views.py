from django.shortcuts import render, redirect
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
from .forms import FriendForm

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    forms = FriendForm(request.POST or None)
    if request.method == 'POST':
        if forms.is_valid():
            forms.save()
            return redirect('/lab-3/')
    
    
    response = {'forms':forms}
    return render(request, 'lab3_form.html', response)