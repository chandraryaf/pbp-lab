from django.shortcuts import render

# Create your views here.

from django.core import serializers
from django.http import HttpResponse
from lab_2.models import Note
from django.shortcuts import render

def index(request):
    data = Note.objects.all() # TODO Implement this
    response = {'note': data}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
