from django.urls import path
from .views import index, xml, json
from lab_4.views import add_note

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
    path('add-note', add_note)
]
